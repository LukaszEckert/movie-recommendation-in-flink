package com.lukasz.eckert.movieFlink

/**
  * Placeholder for prediction results
  * @param input Inputs that where feed into model
  * @param prediction Prediction probability
  * @param time Mean computation time
  */
class PredictionResult(val input: Map[String, String], val prediction: Float, val time: Double) {
}
