package com.lukasz.eckert.movieFlink

import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.core.fs.FileSystem
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.tensorflow.Tensor
import org.apache.flink.streaming.api.scala._

/**
  * Implements a prediction with tensorflow model inside flink.
  *
  * This program reads all data from  files inside folder passed as --data.
  * The easiest way to try this out is to copy new file to this folder.
  * File must consist the same data format as example file. See TODO
  */

object StreamingJob {

  /** Main program method */
  def main(args: Array[String]): Unit = {


    var modelFile: String = ""
    var dataPath: String = ""
    var batchSize = 1

    try {

      val params = ParameterTool.fromArgs(args)
      params.getRequired("model")
      params.getRequired("data")
      modelFile = params.get("model")
      dataPath = params.get("data")
      batchSize = params.getInt("batch", 1)
    } catch {
      case _: Exception =>
        System.err.println("Wrong arguments. Usage:\n" +
          "--model <model> --data <input path>' --batch <batch size>, where <model> is path to folder where model is" +
          "saved pb, <input path> is path to csv input files, <batch size> is batch size that will be used in prediction. Default value for batch size is 1")

        return

    }
    // get the execution environment
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    val movieModel = new MovieModel(modelFile, "serve")

    // get input data by reading from dataPath location
    val text: DataStream[String] = env.readTextFile(dataPath)

    val object_stream = text.map(x => new UserMovie(x)).countWindowAll(batchSize)
    //Prediction stream
    val prediction_stream = object_stream.process(new ProcessModelWindow(movieModel))
    prediction_stream.map(x => "Input: (" + mapToString(x.input) +
      ") probability: " + x.prediction + " in mean time: " + x.time+"ms")
      .writeAsText(dataPath + "output/running_res.txt", FileSystem.WriteMode.OVERWRITE)

    env.execute("Movie prediction stream")
  }

  /**
    * Helper function for creating string from Map[String,String]
    * @param map
    * @return
    */
  def mapToString(map: Map[String, String]): String = {
    var res = ""
    var first = true
    for ((k, v) <- map) {
      if (first)
        first = false
      else
        res += ", "
      res += k + " -> " + v
    }
    res
  }

  /**
    * For testing purposes
    *
    * @param modelPath
    * @param userMovie
    */
  def tensorflow_test(modelPath: String, userMovie: UserMovie): Unit = {
    //Create model object
    val model = new MovieModel(modelPath, "serve")
    //Open
    model.open("predict")

    val userIdTensor = Tensor.create(Array(Array(566L), Array(0L)))
    val movieIdTensor = Tensor.create(Array(Array(1828L), Array(1L)))

    model.feed("userID", userIdTensor)
    model.feed("movieID", movieIdTensor)
    println(model.run())

  }


}
