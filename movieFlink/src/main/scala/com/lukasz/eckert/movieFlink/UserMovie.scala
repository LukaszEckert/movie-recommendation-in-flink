package com.lukasz.eckert.movieFlink

import org.tensorflow.DataType

/**
  * Class for incoming data
  *
  * @param data
  */
class UserMovie(val data: String) {
  protected val columns = "rtAllCriticsScore,rtAudienceScore,rtTopCriticsScore,directorID,user_Action,user_Adventure,user_Animation,user_Children,user_Comedy,user_Crime,user_Documentary,user_Drama,user_Fantasy,user_Film-Noir,user_Horror,user_IMAX,user_Musical,user_Mystery,user_Romance,user_Sci-Fi,user_Short,user_Thriller,user_War,user_Western,movie_genre0,movie_genre1,movie_genre2,movie_genre3,movie_genre4,movie_genre5,movie_genre6,movie_genre7"
    .split(",").toList
  protected val types = List(DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.STRING,
    DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT,
    DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT,
    DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.STRING,
    DataType.STRING, DataType.STRING, DataType.STRING, DataType.STRING, DataType.STRING, DataType.STRING, DataType.STRING)

  protected val dataSplited = data.split(",").toList
  if(dataSplited.size != types.size){
    throw new IllegalArgumentException("Data should have we same size as columns")
  }

  /**
    * Maps each element of data with corresponding column name and type.
    *
    * @return
    */
  def toMapInput(): Map[String, (Array[String], DataType)] = {

    var res = scala.collection.mutable.Map[String, (Array[String], DataType)]()
    val l = List(columns, types, dataSplited)
    val minLenght = l.map(_.size).min
    for (i <- 0 to minLenght - 1) {
      res += (columns(i) -> (Array(dataSplited(i)), types(i)))
    }
    res.toMap
  }


}
