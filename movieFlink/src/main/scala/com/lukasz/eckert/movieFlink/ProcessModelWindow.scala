package com.lukasz.eckert.movieFlink

import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.scala.function.ProcessAllWindowFunction
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow
import org.apache.flink.util.Collector
import org.tensorflow.DataType

import scala.collection.mutable.ListBuffer

class ProcessModelWindow(model: MovieModel) extends ProcessAllWindowFunction[UserMovie, PredictionResult, GlobalWindow] {
  override def open(parameters: Configuration): Unit = {
    super.open(parameters)
    model.open("predict")
  }

  override def close(): Unit = {
    super.close()
    model.close()
  }

  /**
    * Processes window. Creates input map for model prediction. After that it runs model, fetches and prepossesses results.
    * @param context
    * @param elements
    * @param out
    */
  override def process(context: Context, elements: Iterable[UserMovie], out: Collector[PredictionResult]) {

    val it = elements.iterator
    var input = new ListBuffer[Map[String, (Array[String], DataType)]]
    println("Window")
    // Creating big list with all input data in batch
    var count = 0
    while (it.hasNext) {
      val user: UserMovie = it.next()
      val userMapInput = user.toMapInput
      input += userMapInput
      count += 1
    }

    // Flatten map. From List[Map[String, ...]] -> Map[String,List[...]]
    val flatMap = concatMaps(input)


    model.feedMapType(flatMap)
    //Star time
    val start = System.nanoTime()
    val predictions = model.run()
    //End time
    val end = System.nanoTime()

    val tensor = predictions.get("probabilities").get
    val probabilities: Array[Array[Float]] = Array.ofDim[Float](tensor.shape()(0).toInt, tensor.shape()(1).toInt)
    tensor.copyTo(probabilities)

    // Mean time per one userMovie data in milliseconds
    val meanTime = (end - start) / count.toDouble / 1000000.toDouble

    val predictionResult = (probabilities zip input).map(t => t match {
      case (prob, map) => new PredictionResult(map.map(x => x._1 -> x._2._1(0).toString), prob(1), meanTime)
    })
    for (p <- predictionResult) {
      out.collect(p)
    }
    // Close predictions
    predictions.foreach(x => x._2.close())

  }

  /**
    * Transforms  List[Map[String, ...]] -> Map[String,List[...]]
    *
    * @param listBuffer list to transform
    * @return
    */
  def concatMaps(listBuffer: ListBuffer[Map[String, (Array[String], DataType)]]): Map[String, (Array[Array[String]], DataType)] = {
    var res = scala.collection.mutable.Map[String, (Array[Array[String]], DataType)]()
    for ((k, _) <- listBuffer(0)) {
      val list = listBuffer.flatMap(x => x.get(k)).map(x => x._1)
      val columnType = listBuffer(0).get(k).get._2
      res += (k -> (list.toArray, columnType))
    }
    res.toMap
  }
}
