package com.lukasz.eckert.movieFlink

import org.tensorflow.{DataType, Tensor}

/**
  * Helper functions
  */
object Utils {

  /**
    * Convert string to given column type
    * @param string
    * @param columnType
    * @return
    */
  def convertToType(string: String, columnType: DataType) : Any ={

    if(columnType == DataType.INT64)
        return Long2long(string.toLong)
      if(columnType == DataType.DOUBLE)
        return Double2double(string.toDouble)
      if(columnType == DataType.STRING)
        return string.getBytes
  }

  /**
    * Converts string to Long
    * @param string
    * @return
    */
  def convertToInt64(string: String): Long = {
    return string.toLong
  }

  /**
    * Converts string to Float
    * @param string
    * @return
    */
  def convertToFloat(string: String): Float = {
    return string.toFloat
  }

  /**
    * Convert any to Array[any]. If dataType is string then returns Arryy[str.getBytes]
    * @param any
    * @param dataType
    * @return
    */
  def createInputScalar(any: Any, dataType: DataType) : Array[Any]={
    if(dataType == DataType.INT64)
      return Array(any)
    if(dataType == DataType.FLOAT)
      return Array(any)
    return Array(any.asInstanceOf[String].getBytes)
  }

  /**
    * Creates Tensor[java.lang.Long]
    * @param input
    * @return
    */
  def createTensorLong(input : Array[Array[java.lang.Long]]) : Tensor[_]= {
      Tensor.create(input)
  }

}
