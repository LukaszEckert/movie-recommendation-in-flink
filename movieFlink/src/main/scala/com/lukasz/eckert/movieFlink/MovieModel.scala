package com.lukasz.eckert.movieFlink

import com.lukasz.eckert.movieFlink.Utils._
import org.tensorflow.framework.{MetaGraphDef, SignatureDef}
import org.tensorflow.{DataType, SavedModelBundle, Session, Tensor}
import resource._

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer

@SerialVersionUID(1L)
class MovieModel(val modelPath: String, val tags: String*) extends Serializable {
  @transient protected var bundle: SavedModelBundle = _
  @transient protected var runner: Session#Runner = _
  protected var functionName: String = _
  @transient protected var sigDef: SignatureDef = _
  @transient protected var metagraph: MetaGraphDef = _
  @transient protected var tensorsToClose: ListBuffer[Tensor[_]] = _

  /**
    * Loads model from modelPath, setups tags(serve).
    * Assigns sigDef value for given function name.
    * Creates session runner.
    *
    * @param function Name of function inside saved model.
    */
  def open(function: String): MovieModel = {
    bundle = SavedModelBundle.load(modelPath, tags: _*)
    functionName = function
    metagraph = MetaGraphDef.parseFrom(bundle.metaGraphDef())
    sigDef = metagraph.getSignatureDefMap.get(function)
    runner = session().runner()
    tensorsToClose = new ListBuffer[Tensor[_]]()
    this
  }

  /**
    * Closes bundle and all unclosed tensors
    */
  def close(): MovieModel = {
    tensorsToClose.foreach(x => x.close())
    tensorsToClose.clear()

    bundle.close()
    bundle = null
    this
  }

  /**
    * Returns session corresponding to loaded model
    *
    * @return session
    */
  def session(): Session = {
    bundle.session()
  }


  /**
    * Returns signatureDef for given name.
    *
    * @param name Function name
    * @return
    */
  def signatureDef(name: String): Option[SignatureDef] = {
    Option(metagraph.getSignatureDefMap.get(name))
  }

  /**
    * Returns placeholder for given name
    *
    * @param name input name
    * @return String placeholder
    */
  protected def getInputName(name: String): String = {
    sigDef.getInputsMap.get(name).getName
  }

  /**
    * Feed input tensor to session().runner. InputName is translated to corresponding placeholder.
    *
    * @param name   Input tensor name
    * @param tensor Tensor to feed
    * @tparam T tensor type
    */
  def feed[T](name: String, tensor: Tensor[T]): MovieModel = {
    val inputName = getInputName(name)
    runner.feed(inputName.split(":")(0), inputName.split(":")(1).toInt, tensor)

    this
  }

  /**
    * Feed runner with data in form of string converted to given type
    *
    * @param str   input name
    * @param tuple (Array<Array<String>> <- input scalar, DataType <- tensorflow.DataType)
    **/
  def feedType(str: String, tuple: (Array[Array[String]], DataType)): MovieModel = {
    if (tuple._2 == DataType.INT64)
      convertFeedInt64(str, tuple._1)
    if (tuple._2 == DataType.FLOAT)
      convertFeedFloat(str, tuple._1)
    if (tuple._2 == DataType.STRING)
      convertFeedString(str,tuple._1)

    this
  }

  /**
    * Converters Array<Array<String>> to Array<Array<Long>>. Creates tensor that will be feed to runner
    *
    * @param str input name
    * @param array data
    */
  def convertFeedInt64(str: String, array: Array[Array[String]]): Unit = {
    val input: Array[Array[Long]] = array.map(x => x.map(y => convertToInt64(y)))
    val tensor = Tensor.create(input)
    tensorsToClose += tensor
    feed(str,Tensor.create(input))
  }

  /**
    * Converses Array<Array<String>> to Array<Array<Float>>. Creates tensor that will be feed to runner
    *
    * @param str input name
    * @param array data
    */
  def convertFeedFloat(str: String, array: Array[Array[String]]): Unit = {
    val input: Array[Array[Float]] = array.map(x => x.map(y => convertToFloat(y)))
    val tensor = Tensor.create(input)
    tensorsToClose += tensor
    feed(str, Tensor.create(input))
  }

  /**
    * Converters Array<Array<String>> to Array<Array<Array<Byte>>. Creates tensor that will be feed to runner
    *
    * @param str input name
    * @param array data
    */
  def convertFeedString(str: String, array: Array[Array[String]]): Unit = {
    val input: Array[Array[Array[Byte]]] = array.map(x => x.map(y => y.getBytes))
    val tensor = Tensor.create(input)
    tensorsToClose += tensor
    feed(str, Tensor.create(input))

  }
  /**
    * Call feedType for every entry in input
    *
    * @param input map<input name, data to feed>
    * @return
    */
  def feedMapType(input: Map[String, (Array[Array[String]], DataType)]): MovieModel = {
    for ((name, in) <- input) {
      feedType(name, in)
    }
    this
  }

  /**
    * This function starts model execution and return map of output name to output tensor produced by model.
    *
    * @return Map[Syting, Result tensor]
    */
  def run(): Map[String, Tensor[_]] = {
    var out = scala.collection.mutable.Map[String, Tensor[_]]()
    for ((_, v) <- sigDef.getOutputsMap.asScala) {
      runner.fetch(v.getName)

    }
    val res = runner.run().asScala.toList
    for ((k, i) <- sigDef.getOutputsMap.asScala.zipWithIndex) {
      out += (k._1 -> res(i))
    }
    tensorsToClose.foreach(x => x.close())
    tensorsToClose.clear()
    runner = session().runner()
    out.toMap
  }
}
