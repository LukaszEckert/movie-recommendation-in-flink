package com.lukasz.eckert.movieFlink

import org.scalatest.FunSuite
import com.lukasz.eckert.movieFlink.Utils._
import org.tensorflow.DataType
class UtilTests extends FunSuite {

  //convertToInt64
  test("convertToInt64 schould return Long"){
    val in = "5"
    assert(convertToInt64(in).isInstanceOf[Long])
  }
  test("converToInt64 schould return Long with correct value"){
    val in = Array[String]("-1","0","1","15","123523434","-12352314")
    val correct = Array[Long](-1L,0L,1L,15L,123523434L,-12352314L)
    val res = in.map(x => convertToInt64(x))

    assert(res.deep == correct.deep)
  }

  //convertToFloat
  test("convertToFloat schould return Float"){
    val in = "5.2"
    assert(convertToFloat(in).isInstanceOf[Float])
  }
  test("converToFloat schould return Float with correct value"){
    val in = Array[String]("-1.345","0.0","1.123","3.14563","123.23123","-123.15")
    val correct = Array[Float](-1.345f,0.0f,1.123f,3.14563f,123.23123f,-123.15f)
    val res = in.map(x => convertToFloat(x))
    for (i <- 0 to 5){
      assert(correct(i) === res(i))
    }
  }

  test("createInputScalar should return Array"){
    assert(createInputScalar(0L,DataType.INT64).isInstanceOf[Array[_]])
    assert(createInputScalar(0f,DataType.FLOAT).isInstanceOf[Array[_]])
    assert(createInputScalar("a",DataType.STRING).isInstanceOf[Array[_]])

  }
  test("createInputScalar should return Array of lenght 1"){
    assert(createInputScalar(0L,DataType.INT64).length == 1)
    assert(createInputScalar(0f,DataType.FLOAT).length == 1)
    assert(createInputScalar("a",DataType.STRING).length == 1)

  }
  test("createInputScalar shuld return Array with element given in input"+
  " except of string"){
    assert(createInputScalar(0L,DataType.INT64)(0).equals(0L))
    assert(createInputScalar(0f,DataType.FLOAT)(0).equals(0f))

  }
  test("createInputScalar should return Array of Array of bytes for string")
  {

    val res = createInputScalar("abcde",DataType.STRING)(0).asInstanceOf[Array[_]]
    val correct = "abcde".getBytes
    assert(res.deep == correct.deep)
  }

  test("convertToType should create object of given type"){
    assert(convertToType("1", DataType.INT64).isInstanceOf[Long])
    assert(convertToType("1.1", DataType.DOUBLE).isInstanceOf[Double])
    assert(convertToType("abcde", DataType.STRING).isInstanceOf[Array[Byte]])

  }

}