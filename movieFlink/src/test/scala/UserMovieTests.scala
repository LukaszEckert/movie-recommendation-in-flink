import com.lukasz.eckert.movieFlink.UserMovie
import org.scalatest.{FlatSpec, FunSuite}
import org.tensorflow.DataType

class UserMovieTests extends FunSuite{

  test("UserMovie should throw IllegalArgumentException"){
    try{
      val user = new UserMovie("0.1,0.2")
      fail()
    }catch {
      case _ : IllegalArgumentException =>
    }
  }

  test("UserMovie toMapInput should return map"){
    val data ="0.97,0.72,0.96,george_seaton,0.7307262569832402,0.7420765027322405,0.758,0.7602564102564102,0.74,0.7936170212765957,0.8,0.7943107221006565,0.7287037037037037,0.8666666666666666,0.7379310344827587,0.0,0.7666666666666667,0.7894736842105263,0.7714285714285715,0.7251748251748251,0.0,0.7588235294117647,0.8642857142857142,0.7619047619047619,Adventure,Animation,Children,Drama,Sci-Fi,None,None,None"
    val user = new UserMovie(data)
    val map = user.toMapInput()
    assert(map.isInstanceOf[Map[String,(Array[String],DataType)]])
    val sData = data.split(",").toList
    val columns = "rtAllCriticsScore,rtAudienceScore,rtTopCriticsScore,directorID,user_Action,user_Adventure,user_Animation,user_Children,user_Comedy,user_Crime,user_Documentary,user_Drama,user_Fantasy,user_Film-Noir,user_Horror,user_IMAX,user_Musical,user_Mystery,user_Romance,user_Sci-Fi,user_Short,user_Thriller,user_War,user_Western,movie_genre0,movie_genre1,movie_genre2,movie_genre3,movie_genre4,movie_genre5,movie_genre6,movie_genre7"
      .split(",").toList
    val types = List(DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.STRING,
      DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT,
      DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT,
      DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.FLOAT, DataType.STRING,
      DataType.STRING, DataType.STRING, DataType.STRING, DataType.STRING, DataType.STRING, DataType.STRING, DataType.STRING)

    for (i <- 0 until types.size){
      assert(map.contains(columns(i)))
      val tuple = map(columns(i))
      assert(tuple.isInstanceOf[(Array[String], DataType)])
      assert(tuple._2 == types(i))
      assert(tuple._1(0) == sData(i))
    }
  }
}

